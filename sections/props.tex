\section{Propagators}
\label{sec:propagators}

\input{sections/notations}



\begin{definition}[Sync Category]
The \textbf{synchronisation \(\ensuremath{\mathcal{A}} \leftrightarrow \ensuremath{\mathcal{B}}\) of two categories \ensuremath{\mathcal{A}} and \ensuremath{\mathcal{B}} along a span \(\obj \ensuremath{\mathcal{A}}
\xleftarrow{\dom{-}} \corrs \xrightarrow{\cod{-}} \obj \ensuremath{\mathcal{B}}\)} is the category of
related (‘synchronised’) objects and ‘synchronised updates’ between them.
Formally, the category \(\ensuremath{\mathcal{A}} \leftrightarrow \ensuremath{\mathcal{B}}\)
%\begin{wrapfigure}[h]{r}{0.15\textwidth}
%\vspace{-2em}
  \begin{center}
        \begin{tikzcd}
          A \ar[r, leftrightarrow, "r"] \ar[d, dashed, "u"]
          & B \ar[d, dashed, "v"]
          \\ A' \ar[r, leftrightarrow, "r'"]
          &  B'
        \end{tikzcd}
\end{center}
% \vspace{-2.9em}
%\end{wrapfigure}
has objects being tuples \((A : \obj \ensuremath{\mathcal{A}}, B : \obj \ensuremath{\mathcal{B}}, r : A \leftrightarrow B)\); and has arrows
from \((A, B, r)\) to \((A', B', r')\) being pairs of maps \newline \((u : A \to A', v : B \to
B')\) ---thereby yielding the ‘commuting’ diagram to the right. \zdhide{1) the composition of corrs and updates is not defined and so talking about commutativity doesnt make sense. 2) If even we define their composition (it's possible to do if we extend Corr to a profunctor), the diagram would not be commutative -- propagation is not a profunctor (even if Corr is)}.  Identities and
composition are both inherited pointwise from \ensuremath{\mathcal{A}} and \ensuremath{\mathcal{B}} thereby making \(\ensuremath{\mathcal{A}} \leftrightarrow \ensuremath{\mathcal{B}}\) a category.
\zdmoddok{The treatment and analysis of the general setting can be
found in \cite{diskin19:_multip_model_synch_multiar_delta}.}{}{}{-3ex}
\end{definition} \ignorespacesafterend % wrapfigure wont work within a “newtheorem” environment

\newcommand{\sync}{\enma{\mathsf{sync}}}
\zdhide{The def above is inaccurate, in fact, not formal, as you should specify the role of Corr more accurately: is it fixed? varying?. Notation $\spA\longleftrightarrow \spB$ is misleading as it gives an impression that $\longleftrightarrow$ is a binary operation on cats, while actually it essentially uses the corr span. An accurate def should go in this way: Suppose we have a span \spanar{\Corr}{\obj \spA}{\obj \spB} in \setcat, whose elements are called {\em corrs}. Given a corr $r\in\Corr(A,B)$, we call a pair of arrows $(u,v)\in \spA(A,\_)\times \spB(B,\_)$ {\em synchronized} if there is a corr $r'\in \Corr{(\vcod{u},\vcod{v})}$. Then we write \frar{(u,v)}{r}{r'}, and if $r''$ is another such corr, we write \frar{(u,v)}{r}{r''}. In other words, what we actually need is the notion of a {\em sync square}, which is defined to be a quadruple $(r,u,v,r')$ satisfying the incidence conditions shown in the diagram above. Now it is easy to see that sync squares are (``vertically'') composable in the associative way, and $(r,\id,\id,r)$ is the identity square for $r$. This defines category $\sync(\Corr)$ (note that some of the hom-sets can be empty). Thus, any span \spanar{\Corr}{\obj \spA}{\obj \spB} gives rise to a cat $\sync(\Corr)$.
\\
Now, when we defined this \sync\ accurately, it makes sense to ask whether the construction compositional, ie, if \sync\ is a functor into \catcat. For this, we need to define functor \frar{\obj}{\catcat}{Setcat},
compose it with span-formation functor and then apply \sync. And all this is not needed for us (at least, not immediately needed). So, your def 3.1 is (not only semi-formal but also) misleading the reader and not needed.
\\
Talking about \sync\ would be much more useful after the notion of propagator is introduced. Then we say that any prop $\ppg$ gives rise to a cat $\sync\Corr$ whose arrows are pairs $(u,\ppg(u) )$, and $\sync(\Corr)(A,A')\neq\varnothing$ if $\spA(A,A')\neq \varnothing$.
}

\zdhide{So, the right sequence is first to introduce propagators and their composition, and then define functor \sync\ if needed. Using \sync\ to motivate props is both misleading and doesnt do the formal job we need.}

Notice that we obtain a \emph{domain functor} \(\dom{\_{}} : (\ensuremath{\mathcal{A}} \leftrightarrow \ensuremath{\mathcal{B}}) \to \ensuremath{\mathcal{A}}\) by keeping
only the left-most data: On objects \(\dom{(A, B, r)} = A\) and on arrows
\(\dom{(u, v)} = u\).  The problem of \emph{propagation} is then: Given any synchronised
pair \(s \in \obj (\ensuremath{\mathcal{A}} \leftrightarrow \ensuremath{\mathcal{B}})\), how do we lift updates \(u \,:\, \dom{s} \to A' \in \arr \ensuremath{\mathcal{A}}\)
of one component to synchronised updates \(f : s \to s' \in \arr (\ensuremath{\mathcal{A}} \leftrightarrow \ensuremath{\mathcal{B}})\) on the pair
---i.e., such that \(\dom{(s')} = A'\) and \(\dom{f} = u\).  We call such a lift
operation a \textbf{propogator}; it is essentially a generalisation of the concept of
opfibration ---namely, it is a opfibration without universality.  Explicitly, to
define a propogator, we need two families of total functions:
\begin{align*}
            \ppg^r &: \spA(\dom{r},\_) \to \spB(\cod{r},\_)
          & \quad \text{ indexed by } r\in \corrs
        \\
          \ppg_u &: \corrs(\dom{u},\_) \to \corrs(\cod{u},\_)
          & \quad \text{ indexed by } u\in\arr{\spA}
\end{align*}
        such that the following incidence condition holds:
        \[
          \text{for all $r, u$: } \;\;
          \dom{u}=\,\dom{r}
          \quad\Rightarrow\quad
          \cod{(\ppg^r(u))}=\cod{(\ppg_u(r))}
        \]
        Now we set $\ppg(r, u) \eqdef ( \ppg_u\, r, \ppg^r\, u)$.

The algebraic definition is bulky since it requires necessary incidence
conditions for nodes and arrows. A more elegant ‘diagrammatic definition’
\cite{diskin_stunkel20:universal_algebra_of_diagrammatic_operations} of
propogation is as follows.

\begin{definition}[Propagator]
A \textbf{propagator} \emph{from \(\spA\) to \spB}, written as  \(\ppgrab\)
\end{definition} \ignorespacesafterend % wrapfigure wont work within a “definition”
consists of a span \(\obj\spA \xleftarrow{\dom{-}} \corrs
\xrightarrow{\cod{-}} \obj\spB\) of sets, \zdmoddok{- ‘corrs’, between the objects in the categories,}{the elements of the apex are called \emph{correspondences} or {\em corrs}}{[it's awkward phrasing to be replaced]}{-3ex},
and a diagrammatic operation \(\ppg\)
\begin{wrapfigure}{r}{0.15\textwidth}
\vspace{-2em}
  \begin{center}
    \begin{tikzcd}
          \dbox{A} \ar[r, leftrightarrow, "r"] \ar[d, "u"]
          % \drar[phantom, "\quad\quad\rotatebox{-35}{$\xMapsto\ppg$}\quad"]
          \drar[phantom, Mapsto, "{:}\ppg"]
          & \dbox{B} \ar[d, dashed, "v", blue]
          \\ \dbox{A'} \ar[r, leftrightarrow, dashed, "r'", blue]
          &  {\color{blue} B'  }
        \end{tikzcd}
\end{center}
\vspace{-3em}
\end{wrapfigure}
that takes any pair \((r,u)\) sharing a common source as shown in the diagram and
completes it with two arrows \((r', v) = (\ppg_u\, r, \ppg^r\, u)\) to form the
square diagram as shown.  In this diagram, framed nodes and solid arrows denote
elements that are taken by operation \(\ppg\) as an input, and the unframed node
and dashed arrows are the output. We set \(( \ppg_u\, r, \ppg^r\, u) \eqdef
\ppg(r, u)\).

\zdhide{So, all piece is to be rewritten in the right order. First, u define props as it was defined in the version long ago that I edited. Then you define cat \sync(\ppg). Then you should investigate compostionality: how $\sync(\ppg_1;\ppg_2)$ and $\sync(\ppg_i)$ are related.}

%\clearpage
\begin{definition}[Properties]
\iffalse Below is listing of some useful properties that are enjoyed by some propogators
but not necessarily by all propogators. \fi
\vspace{-2em}
\begin{center}
\begin{tabular}{l|l}
    Property & Definition \\
    \hline\hline
    \emph{Total} & The left leg, $\dom{-}$, is surjective \\ \hline
    \emph{Surjective} & The right leg, $\cod{-}$, is surjective \\ \hline % (right total)
    \emph{Full} & Every function $\ppg^r$ is surjective \\ \hline
    \emph{Faithful} & Every function $\ppg^r$ is injective \\ \hline
    \emph{Boolean} & % $R$ is an ordinary relation\footnote
                     Every fiber $\corrs(A,B)$ is either empty or a singleton \\ \hline
    \emph{Stable} &  $\ppg(r, \Id({\dom{r}})) = (r, \Id({\cod{r}}))$ for all
                    $r$ \\\hline
    \parbox{2.8cm}{\emph{Compositional}} % {\rm  see  \figref{ppgop}(c)}}}
     & $\ppg(r, u) = (r', v) \text{ and } \ppg(r', u') = (r'', v') \;\Rightarrow\; \ppg(r, u \, ; \, u') = (r'',
       v \, ; \, v')$
       % \\ &
       %    $\ppg(r, u\, ; \, u') = \big( r.\ppg_u.\ppg_{u'},\; \ppg^r(u)\, ; \,\ppg^{r'}(u') \big){\rm ~see~\figref{ppgop}(c)}$

                           \\\hline
    \emph{Functorial} & Stable and compositional % \\ \hline
    % \emph{has lift G} & $G : \Obj \spB \to \Obj \spA$
    %                    and $\corrs(A, B) = \big(\IF{A = G(B)}{\{*\}}{\{\}}\big)$
  \end{tabular}
\end{center}
\end{definition}
\vspace{1em} \begin{example}
The construction of propagator has a (surprisingly) wide range of instances in
several domains.
\begin{enumerate}
\item (Behaviour Modelling) A \emph{Mealy machine} \ensuremath{\mathcal{M}} is given by a span
\(\mathsf{Ouptut} \xleftarrow{o} \mathsf{State} × \mathsf{Input}
   \xrightarrow{\ensuremath{\mathcal{d}}} \mathsf{State}\) along with an ‘start state’ \(\ensuremath{\iota} \in
   \mathsf{State}\).  The homomorphic extension of \(o, \ensuremath{\mathcal{d}}\) to sequences provides a
propogator from the free monoid of inputs to the free monoid of outputs
---each considered as a one-object category.
This is an instance of a \emph{module} \cite{DBLP:conf/popl/HofmannPW12},
and the example is inspired from \cite{DBLP:journals/mscs/BungeF00}.

\item (“Functors as propagators”) Every functor \(F : \ensuremath{\mathcal{A}} \to \ensuremath{\mathcal{B}}\) gives rise
to a propogator \(\ensuremath{\mathcal{A}} \to \ensuremath{\mathcal{B}}\), which we will also denote by \(F\),
with span \(\Obj \ensuremath{\mathcal{A}} \xleftarrow{\Id} \Obj \ensuremath{\mathcal{A}} \xrightarrow{F} \Obj \ensuremath{\mathcal{B}}\)
and \(\ppg(A, u) = (\cod{u}, F\, u)\).
It is obviously a total functorial Boolean propogator.  Moreover, it is easy
to see that the propagator \(F\) is full/faithful/surjective iff the functor
\(F\) is such. Hence, we have a functor \(\ensuremath{\iota} : \ensuremath{Cat} \hookrightarrow Ppg\) from
the category of small categories to the category of propogators
(defined below).

A \emph{functorial} propagator \ppgrab between one-object categories ---i.e., \(\spA\)
and \(\spB\) are monoids--- is also known as a \emph{stateful monoid homomorphism}
\cite{DBLP:conf/popl/HofmannPW12}.

\item (Opfibrations) Every opfibration \(F : \ensuremath{\mathcal{A}} \to \ensuremath{\mathcal{B}}\) with cleavage \(u \mapsto \bar{u}\)
gives rise to a propagator \(\lambda_F : \ensuremath{\mathcal{A}} ← \ensuremath{\mathcal{B}}\) ---note the direction reversal---
with span \(\Obj \ensuremath{\mathcal{A}} \xleftarrow{\Id} \Obj \ensuremath{\mathcal{A}} \xrightarrow{F} \Obj \ensuremath{\mathcal{B}}\) and
\(\ppg(r, u) = (\bar{u}, \cod{u})\).  It is clearly a surjective Boolean
propogator.  Moreover, well known properties \cite{JacobsCLTT} of fibrations
imply that \(\lambda_F\) is stable and compositional, and is thus functorial.
Furthermore, \(\lambda_F\) is total iff \(F\) is surjective on objects.

\item (Graph Transformations)
Graph rewriting gives rise to a propogator since opfibrations are propogators
and graph rewriting is an opfibration \cite{Banach_2002}.
\end{enumerate}

\end{example}
\begin{theorem}
Propagators form a symmetric monoidal category \ensuremath{Ppg}.
\end{theorem}

\noindent
\emph{Proof:} Given a category \(\spA\), the identity propagator \(\id_\spA :
\spA \to \spA\) is the propagator
\begin{wrapfigure}{r}{0.3\textwidth}
\vspace{-2.5em}
  \begin{center}
\begin{tikzcd}
  A \ar[rr,leftrightarrow, bend left, "r"{name=X}]
    \ar[r, leftrightarrow, "r_1"{name=Y}, gray]
    \ar[mapsto, dashed, gray, from=X, to=Y]
    \ar[d, "u"]
    % \drar[phantom, "\quad\quad\rotatebox{-35}{$\xMapsto{\mathsf{P}}$}\quad"]
    \drar[phantom, Mapsto, "P"]
 & {\color{gray} B}
     \ar[d, dashed, "u'", gray]
     \ar[r, leftrightarrow, "r_2"{name=Z}, gray]
     \ar[mapsto, dashed, gray, from=X, to=Z]
     % \drar[phantom, "\quad\quad\rotatebox{-35}{$\xMapsto{\mathsf{Q}}$}\quad"]
     \drar[phantom, Mapsto, "Q"]
 & C \ar[d, dashed, "u''", blue]
\\  A'
   \rar[leftrightarrow, dashed, "r_1'"{name=YL, below}, dashed, gray]
   \ar[rr,bend right, leftrightarrow, "r'"{name=XL, below}, dashed, blue]
&  {\color{gray} B'}
    \rar[dashed, leftrightarrow, "r_2'"{name=ZL, below}, gray]
& {\color{blue} C'}
   \ar[mapsto, dashed, gray, from=XL, to=YL]
   \ar[mapsto, dashed, gray, from=XL, to=ZL]
\end{tikzcd}
\end{center}
\vspace{-3em}
\end{wrapfigure}
generated by the identity functor ---since functors are propogators.  The
composition of propagators \(\ensuremath{\mathcal{A}} \xrightarrow{P} \ensuremath{\mathcal{B}} \xrightarrow{Q} \ensuremath{\mathcal{C}}\) is given by
‘pasting diagrams’: One begins ‘reading’ from the black top-left, derives the
grey in the middle, then obtains the required blue in the bottom-right.  An
algebraic definition is as follows; where associativity is inherited from the
associativity of span composition.
\begin{align*}
  (P \, ; \, Q).\corrs &= \{(r_1, r_2) \in P.\corrs × Q.\corrs \,|\, {r_1}^{P.•} \;=\; {}^{Q.•}{r_2}\}
  \\ (P\, ; \,Q).\ppg^{(r_1, r_2)}(u)
                &= Q.\ppg^{r_2}\big(P.\ppg^{r_1}(u)\big)
  \\ (P\, ; \,Q).\ppg_u(r_1, r_2)
                &= \big(P.\ppg_u(r_1),\, Q.\ppg_{P.\ppg^{r_1}(u)}(r_2)\big)
\end{align*}

Moreover, the \zdmoddok{monoidal tensor is the \textbf{parallel composition} of propogators; which }{monoidal (or tensor) product}{}{-2ex} is \emph{induced} by the
usual Cartesian product structure of \ensuremath{Cat}. To wit, given two props \(P : \ensuremath{\mathcal{A}} \to \ensuremath{\mathcal{C}}, Q : \ensuremath{\mathcal{B}} \to \ensuremath{\mathcal{D}}\), their {\em parallel composition} is defined to be the
prop \(P \,\mid\mid\, Q : \ensuremath{\mathcal{A}} × \ensuremath{\mathcal{B}} \to \ensuremath{\mathcal{C}} × \ensuremath{\mathcal{D}}\) with the following data:
\begin{align*}
  (P \,\mid\mid\, Q).\corrs &= P.\corrs × Q.\corrs
  \\ (P \,\mid\mid\, Q).\ppg^{(r_1, r_2)}(u)
                &= \big(P.\ppg^{r_1}(u),\, Q.\ppg^{r_2}(u) \big)
  \\ (P \,\mid\mid\, Q).\ppg_u(r_1, r_2)
                &= \big(P.\ppg_u(r_1),\, Q.\ppg_u(r_2)\big)
\end{align*}
Since \(P \,\mid\mid\, Q\) is obtained by “sticking diagrams side-by-side”, it is clearly a
propogator whenever \(P\) and \(Q\) are such.
\zdhide{To show that parallel composition is actually a monoidal product, we [fill-in pls]..... }{}
The monoidal product of
objects is the Cartesian product of categories, and the monoidal product of
arrows is their parallel composition; the monoidal unit is the terminal category
\ensuremath{\mathbf{1}}.  From example 3.1 we know that functors are propogators and so the required
monoidal associator, unitor, and braiding isomorphisms arise as propogators from
the respective isomorphism functors of Cartesian products in \ensuremath{Cat} (which is
itself a monoidal category in which the required equations hold).
(See \cite{DBLP:conf/fossacs/Diskin20} for a similar proof.)
