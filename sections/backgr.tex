\section{Background: Generalized Differentiation via Change Action Structures}

The content of this section comes from \cite{DBLP:conf/fossacs/Alvarez-Picallo19}.

\subsection{Basic definitions}
\begin{definition}[Change Action]
  A \emph{(codiscrete) change action} is a tuple $A = (|A|, \Delta A, \oplus_A, +_A, 0_A)$
  where $|A|$ and $\Delta A$ are sets, $(\Delta A, +_A, 0_A)$ is a monoid, and $\oplus_A :
  |A| \times \Delta A \to |A|$ is an action of the monoid on $|A|$.

 We omit the subscript from $\oplus_A, +_A, 0_A$ whenever we can.
\end{definition}

\begin{definition}[Derivative condition]
Let $A$ and $B$ be change actions. A function $f : |A| \to |B|$ is \emph{differentiable}
if there is a function $\partial f : |A| \times \Delta A \to \Delta B$ satisfying \[f(a \oplus_A \delta a) = f(a)
\oplus_B \partial f(a, \delta a)\] for all $a \in  |A|, \delta a \in  \Delta A$. We call $\partial f$ a derivative
for $f$, and write $f : A \to B$ whenever $f$ is differentiable.
\end{definition}

\begin{theorem}[Chain Rule]
Given $f : A \to B$ and $g : B \to C$ with derivatives $\partial f$ and $\partial g$ respectively,
the function $\partial (g \circ f) : |A| \times \Delta A \to \Delta C$ defined by $\partial (g \circ f)(a, \delta a) := \partial
g(f(a), \partial f(a, \delta a))$ is a derivative for $g \circ f : |A|\to|C|$.
\end{theorem}

\begin{definition}[Regular Derivatives]
Given a differentiable map $f : A \to B$, a derivative $\partial f$ for $f$ is \emph{regular}
if, for all $a \in  |A|$ and $\delta a, \delta b \in  \Delta A$, we have $f(a, 0_A) = 0_B$ and $\partial
f(a, \delta a +_A \delta b) = \partial f(a, \delta a) +_B \partial f(a \oplus_A \delta a, \delta b)$.

In the lens setting, this becomes stability and compositionality requirement.
\end{definition}

\begin{theorem}[Uniqueness implies Regularity]
Whenever $f : A \to B$ is differentiable and has a unique derivative $\partial f$, this
derivative is regular.
\end{theorem}

A more constructive approach is to consider morphism as a function together with
a choice of a derivative for it.

\begin{definition}[Differential Map]
Given change actions $A$ and $B$, a differential map $f : A \to B$ is a pair $(|f|,
\partial f)$ where $|f| : |A|\to|B|$ is a function, and $\partial f : |A| \times \Delta A \to \Delta B$ is
a regular derivative for $|f|$.
\end{definition}

The category \emph{CAct} has change actions as objects and differential maps as
morphisms. The identity morphisms are $(Id_A, \pi _1)$; given morphisms $f : A \to B$
and $g : B \to C$, define the composite $g\circ f := (|g|\circ |f|, \partial g\circ \langle |f| \circ \pi_1, \partial f\rangle)
: A \to C$.

Finite products and coproducts exist in \emph{CAct}. Whether limits and colimits
exist in \emph{CAct} beyond products and coproducts is open.

If one thinks of changes (i.e. elements of $\Delta A$) as morphisms between elements
of $|A|$, then regularity resembles functoriality. In fact, categories of change
actions organise themselves into 2-categories.

The definition of change actions makes no use of any properties of \emph{Set}
beyond the existence of products. Indeed, change actions can be characterised as
just a kind of multi-sorted algebra, which is definable in any category with
products.

\textbf{The Category CAct(C)}. Consider the category $Cat_\times$ of (small)
cartesian categories (i.e. categories with chosen finite products) and
product-preserving functors. We can define an endofunctor $CAct : Cat_\times \to Cat_\times$
sending a category $C$ to the category of (internal) change actions on $C$.

The objects of $CAct(C)$ are tuples $A = (|A|, \Delta A, \oplus_A, +_A, 0_A)$ where $|A|$
and $\Delta A$ are (arbitrary) objects in $C$, $(\Delta A, +_A, 0_A)$ is a monoid object
in $C$, and $\oplus_A : |A| \times \Delta A \to |A|$ is a monoid action in $C$, i.e. a
C-morphism satisfying, for all $a : C \to |A|, \delta_1 a, \delta_2 a : C \to \Delta A$:
\[ \oplus_A \circ \langle a, 0_a \circ !\rangle = a\]
\[ \oplus_A \circ \langle a, +_A \circ \langle \delta_1 a, \delta_2 a \rangle \rangle = \oplus_A \circ \langle\oplus_A \circ \langle a, \delta_1 a \rangle, \delta_2 a
\rangle \]

Given objects $A, B$ in $CAct(C)$, the morphisms of $CAct(A, B)$ are pairs $f =
(|f|, \partial f)$ where $|f| : |A|\to|B|$ and $\partial f : |A| \times \Delta A \to \Delta B$ are morphisms in
$C$, satisfying a diagrammatic version of the derivative condition:
\def\COMMA{,}
\begin{tikzcd}
  {|A| \times \Delta A} \rar["\qquad\langle |f| \circ \pi_1 \COMMA \partial f\rangle\qquad"] \dar["\oplus_A"] & {|B| \times \Delta B} \dar["\oplus_B"]
\\ {|A|} \rar["|f|"] & {|B|}
\end{tikzcd}

Additionally, we require our derivatives to be regular.

\subsection{Basic differential geometry}

%\textbf{Tangent Bundles in Change Action Models.} 
In differential geometry the tangent bundle functor, which maps every manifold to its tangent bundle, is an important construction. There is an endofunctor on change action models reminiscent of the tangent bundle functor, with analogous properties.

\begin{definition}[Tangent Bundle Functor]
The tangent bundle functor $T : C \to C$ is defined as $T\,A := A \times \Delta A$ and $T\, f :=
\langle f \circ \pi_1, \partial f \rangle$.
\end{definition}

Notation. We use shorthand $\pi_{ij} := \pi_i \circ  \pi_j$ .

A particularly interesting class of change action models are those that are also
cartesian closed. Surprisingly, this has as an immediate consequence that
differentiation is itself internal to the category.

\begin{theorem}[Internalisation of derivatives]
Whenever $C$ is cartesian closed, there is a morphism $d_{A,B} : (A \Rightarrow B) \to (A \times
\Delta A) \Rightarrow \Delta B$ such that, for any morphism $f : 1 \times A \to B$, $d_{A,B} \circ \Lambda f = \Lambda (\partial f
\circ \langle\langle\pi_1, \pi_{12}\rangle, \pi_1, \pi_{22}\rangle\rangle)$.
\end{theorem}


\begin{definition}[Infinitesimal Object]
If $C$ is cartesian closed, an \emph{infinitesimal object} $D$ is an object of
$C$ such that the tangent bundle functor $T$ is represented by the covariant
Hom-functor $D \Rightarrow (\cdot )$, i.e. there is a natural isomorphism $\phi  : (D \Rightarrow (\cdot )) \to
T$.
\end{definition}

\begin{theorem}
Whenever there is an infinitesimal object in $C$, the tangent bundle
$T(A \Rightarrow  B)$ is naturally isomorphic to $A \Rightarrow  T\,B$.
\end{theorem}

We would like the tangent bundle functor to preserve the exponential structure;
in particular we would expect a result of the form $\frac{\partial (\lambda y.t)}{\partial x} = \lambda
y. \frac{\partial t} {\partial x}$, which is true in differential $\lambda$-calculus
\cite{diff_LC}. Unfortunately it seems impossible to prove in general that this equation
holds, although weaker results are available. If the tangent bundle functor is
representable, however, additional structure is preserved.

\begin{theorem}
The isomorphism between the functors $T(A \Rightarrow (\cdot ))$ and $A \Rightarrow T(\cdot )$ respects the
structure of $T$, in the sense that the diagram commutes.
\begin{tikzcd}
  T(A \Rightarrow B) \dar["\oplus_{A \Rightarrow B}"] \rar["\cong"] & A \Rightarrow T(B) \ar[dl, "Id_A \Rightarrow \oplus_B"]
\\ A \Rightarrow B
\end{tikzcd}
\end{theorem}

\textbf{Difference Calculus and Boolean Differential Calculus.}

In the calculus of finite differences [15], the discrete derivative (or discrete
difference operator ) of a function $f : Z \to Z$ is defined as $\delta f(x) := f(x +
1) - f(x)$. In fact the discrete derivative $\delta f$ is (an instance of) the
derivative of $f$ qua morphism in $GrpSet$, i.e. $\delta f(x) = \partial f(x, 1)$.

Finite difference calculus \cite{finite_calculus, finite_diff} has found
applications in combinatorics and numerical computation. Our formulation via
change action model over GrpSet has several advantages. First it justifies the
chain rule, which seems new. Secondly, it generalises the calculus to arbitrary
groups. To illustrate this, consider Boolean differential calculus
\cite{Boolean_diff_calculus, Boolean_finite_calculus}, a technique that applies
methods from calculus to the space $\mathbb{B}^n$ of vectors of elements of some
Boolean algebra $\mathbb{B}$.

\textbf{In conclusion} (we directly quote \cite{DBLP:conf/fossacs/Alvarez-Picallo19}), change actions and change action models constitute a new setting for reasoning about differentiation that is able to unify “discrete” and “continuous” models, as well as higher-order functions. Change actions are remarkably well-behaved and show tantalising connections with geometry and 2-categories. We believe that most ad hoc notions of derivatives found in disparate subjects can be elegantly integrated into the framework of change action models. We therefore expect any further work in this area to have the potential of benefiting these notions of derivatives.

\subsection{Edit lenses} [To be completed]
