\subsection{Notation and Terminology}
    \label{sec:orgab7ee54}
    We will use the notation \(\obj{\spXX}\) to refer to the
    objects of a category \spXX, and \(\Arr\spXX\) to refer
    to its arrows; we also use
    \(\dom{\_},\cod{\_},\; \_{}\, ; \,\_{},\;
    \Id\_\) to refer to the source and the target object assignment, the
    (forward/diagrammatic) composition operation, and for the identity operations,
    respectively.  We write \(X \to_\spXX Y\) for morphisms of category \(\spXX\) whose
    source is \(X\) and target is \(Y\); we omit the subscript when it is clear from
    context.  We often write ``object \(A\,{:}\,\spA\)'' and ``arrow \(a{:}\,\spA\)'',
    and may even omit nouns ``object'' and ``arrow'' when the context make its
    clear.
    When working with compound structures, say of type \(\mathcal{S}\), that have fields \emph{named}
    \(f\), say of type \(\mathcal{T}\), then we write \(s.f\) to refer to that field within an
    \(\mathcal{S}\)-structure \(s\). If \(\mathcal{S}\) and \(\mathcal{T}\) can be shown to be categories and the projection \(s
    \mapsto s.f\) is functorial, we will use the \emph{name} \(f\) without any further decoration to
    refer to the associated forgetful functor \(\mathcal{S} \to \mathcal{T}\).
    {\renewcommand{\zd}[1]{}
    \begin{definition}[Span \cite{DBLP:journals/entcs/BruniG01}]
        In a category \ensuremath{\mathcal{C}}, a \emph{span} is a pair of arrows that share a common source; for a
        span \(X \xleftarrow{l} A \xrightarrow{r} Y\) one refers to \(A, l, r\) as the \emph{apex,
            left leg, right leg}, resp, and $X,Y$ as the {\em source} and the {\em target} {\em feet} resp.
        We will often denote both a span and its apex by the same letter, and write \spanar{A}{X}{Y}. When \ensuremath{\mathcal{C}} has pullbacks, spans can be composed
        to form a bicategory, but if we consider spans up to isomorphism, span
        composition is strictly associative and we thus have a category
        \(\mathsf{Span}(\ensuremath{\mathcal{C}})\). %(see \cite{?}[Bruni])

        Given a span \spanar{A}{X}{Y} in \(Set\) \zdnewok{and a pair $(x,y)\in X\times Y$,}{} we define the \emph{fiber} \newline \[A(x, y) =
        \{a \in A \,|\, l\, a = x \text{ and } \, r\, a = y\}\] and whenever \(a \in A(x, y)\) we
        write \(a : x \leftrightarrow y\) and say that \emph{\(x\) and \(y\) are related by \(a\)}.
    \end{definition}
    \zd{your notation $l,r$ for legs is not good b/c in lenses $r$ denotes corrs. So, you should change the above by denoting the span by $R$ and its legs somehow differently. Why not by \hdom{\_}, \hcod{\_}? }
}
    \begin{definition}[Wide subcategory]
        A subcategory \ensuremath{\mathcal{A}} of \ensuremath{\mathcal{B}} is \emph{wide} if \ensuremath{\mathcal{A}} contains all objects of \ensuremath{\mathcal{B}}, and a functor is a
        \emph{wide embedding} if it is faithful and bijective-on-objects.
    \end{definition}
    For example, \zdnewok{for any category $C$}{} we have
    wide embedding \(\mathsf{span}_C:  C \hookrightarrow \Span C\)
    %{\frar{\mathsf{Span}}{\catcat}{\catcat}}{}{-3ex}
    that sends objects to themselves and arrows to identity-paired spans: \(\big(X \xrightarrow{f} Y\big) \,\mapsto\, \big(X
    \xleftarrow{\id_X} X \xrightarrow{f} Y\big)\); and so we may view \ensuremath{{C}} as a wide
    subcategory of \(\Span \ensuremath{{C}}\).

    \zdmoddok{whence the latter may \emph{inherit structure} from the
        former as shown in the following two results.  Similarly, we have the functor
        \(\ensuremath{\mathcal{O}} : \ensuremath{\mathcal{C}} \to \Span Set\) that sends categories to their object \emph{sets} and functors to
        identity-paired spans.}{}{}{-5ex}
    \begin{theorem}[$\mathsf{Span}(\ensuremath{\mathcal{C}})$ inherits (symmetric) monoidal structure from \ensuremath{\mathcal{C}}]
        If \((\ensuremath{\mathcal{C}}, \otimes, \ensuremath{\mathcal{I}})\) is a (symmetric) monoidal category with pullbacks, then so is
        \(\mathsf{Span}(\ensuremath{\mathcal{C}})\) \emph{provided} the tensor preserves pullbacks: If \(P_i\) is a
        pullback of \(A_i \to O_i ← B_i\), for \(i \in 1..2\), then \(P_1 \otimes P_2\) is a pullback for \(A_1
        \otimes A_2 \to O_1 \otimes O_2 ← B_1 \otimes B_2\); i.e., “pullbacks can be formed componentwise”: \((A_1 \otimes
        A_2) ×_{O_1 \otimes O_2} (B_1 \otimes B_2) \cong (A_1 ×_{O_1} B_1) \otimes (A_1 ×_{O_2} B_2)\).
    \end{theorem}
    %\zd{It'd a bad style to combine a theorem and its proof.}
    \emph{Proof}. We sketch the required construction. The tensor on objects is their tensor in \ensuremath{\mathcal{C}}, the monoidal unit is \ensuremath{\mathcal{C}}'s unit object \ensuremath{\mathcal{I}}, and given two spans \(X_i \xleftarrow{l_i} A_i \xrightarrow{r_i} Y_i\), for \(i
    \in 1..2\), their tensor is \emph{pointwise} \(X_1 \otimes X_2 \xleftarrow{l_1 \otimes l_2} A_1 \otimes A_2
    \xrightarrow{r_1 \otimes r_2} Y_1 \otimes Y_2\). In addition, if \ensuremath{\mathcal{C}} has a symmetry
    \(\mathsf{X}_{A, B} : A \otimes B \cong B \otimes A\), then so does \(\mathsf{Span}(\ensuremath{\mathcal{C}})\) with the
    symmetry at \(A\) and \(B\) being the span \(A \otimes B \xleftarrow{\id_A \otimes \id_B} A \otimes B
    \xrightarrow{\mathsf{X}_{A,B}} B \otimes A\). \zdhide{am not sure it's the right constr. I'd expect to apply symmetry to the apex and swap the left-right legs.}Finally, the embedding \(\Gamma : \ensuremath{\mathcal{C}} \to \Span \ensuremath{\mathcal{C}}\)
    is a strict monoidal functor. \zdhide{why? if it's easy to see, write so} In particular, if \ensuremath{\mathcal{C}} has finite products ---which
    preserve pullbacks since limits commute with limits--- then \(\mathsf{Span}(\ensuremath{\mathcal{C}})\)
    inherits that monoidal structure.\zdhide{i didnt think about this but it sounds a bit strange}
  %  \zd{Extensivity theworem is commented here}
%    \begin{theorem}[\cite{lindner_remarks_on_mackey_functors} $\mathsf{Span}(\ensuremath{\mathcal{C}})$ inherits biproducts from \ensuremath{\mathcal{C}}'s sums]
%        The coproduct of extensive distributive categories \ensuremath{\mathcal{C}} gives rise to a biproduct
%        in the span category \(\Span \ensuremath{\mathcal{C}}\). In particular, sums and products coincide since
%        there is an identity-on-objects functor between span categories and their
%        opposites: Every span \(S = \big(X \xleftarrow{l} A \xrightarrow{r} Y\big)\) has a
%        \emph{dual} \(S^\dagger = \big(Y \xleftarrow{r} A \xrightarrow{l} X\big)\) obtained by
%        swapping the legs.
%    \end{theorem}
%    \emph{Remark}: Extensivity ensures that sums interact well with pullbacks and
%    essentially means that sums behave as they do in the category of sets.
%
%    \zd{why on earth we need this theorem???}
