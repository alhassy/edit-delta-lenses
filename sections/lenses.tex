\section{Lenses}
\label{sec:org64f2702}
\label{sec:lenses}

\subsection{Symmetric lenses}
\begin{definition}[Symmetric Lens]
A \textbf{(symmetric) lens} is a pair of propagators (the \emph{forward} and the \emph{backward})
\fppgrab and \bppgrab working in the opposite directions over the same
space of corrs \(\Obj \ensuremath{\mathcal{A}} \xleftarrow{\;} \corrs \xrightarrow{\;} \Obj \ensuremath{\mathcal{B}}\) ---but
note that for \(\bppg\) the corr span is inverted.
\end{definition}

\noindent \begin{definition}[Lens Properties]
For any of  propagator properties \ensuremath{\mathcal{P}} \ensuremath{\in} \{total, surjective,
full, \ldots{}\}, we say “a lens is left-\ensuremath{\mathcal{P}} (or right-\ensuremath{\mathcal{P}})” if “\fppg (respectively
\bppg) has property \ensuremath{\mathcal{P}}”.  Furthermore, we say “a lens is \ensuremath{\mathcal{P}}” when it is
both left-\ensuremath{\mathcal{P}} and right-\ensuremath{\mathcal{P}}.
\end{definition}
\vspace{1em} \begin{example}
\begin{enumerate}
\item (Behaviour Modelling) We have a propogator in the opposite direction
\(\mathsf{State}^* ← \mathsf{Ouptut}^*\): Given any sequence of outputs, use
the axiom of choice to find an input and reachable state such that the
machine yields that sequence of outputs; if no input-state pair determines a
certain output, stop and yield the sequence of inputs obtained so far.

\item (Cloven opfibrations)
By the previous example, an opfibration \frar{F}{\spA}{\spB} gives us a
pair of propagators \((F, \lambda_F)\) working in the opposite
directions over the same (Boolean) space of corrs.
\end{enumerate}

\end{example}
\begin{theorem}
Lenses form a symmetric monoidal category \ensuremath{Lens}, whose objects are small categories
and whose arrows are lenses.
\end{theorem}

\noindent \emph{Proof:} We first notice that \ensuremath{Lens} is definable by pullback, and so
inherits its claimed structure this way.
\begin{tikzcd}
        \ensuremath{Lens} \rar[dashed, "\fppg"] \dar[dashed, "\bppg"] \PB
        & \ensuremath{Ppg} \dar["\corrs"]
        \\ \ensuremath{Ppg} \rar["\corrs\circ \dagger"] & \mathsf{Span}(\ensuremath{Set})
\end{tikzcd}

\subsection{Asymmetric lenses}
\begin{definition}[Asymmetric Lens]
An \textbf{asymmetric lens (a-lens)} is a lens whose forward propagator is \emph{induced} by a
functor \(\getl : \ensuremath{\mathcal{A}}\to \ensuremath{\mathcal{B}}\) (read “get the view”) ---recall that functors are
propogators. We will normally denote such lenses by
\(\alensrar{(\getl,\putl)}{\corrs}{\spA}{\spB}\) (reading \(\putl\) as “put the view
update back”).
\end{definition}

\begin{theorem}
A-lenses form a wide subcategory \(\alenscat\) of \(\lenscat\).
\end{theorem}

\noindent
\emph{Proof:}
Since the composition of two \(\mathsf{get}\)'s as propogators is precisely
their composition as functors, the composition of two lenses is given by
usual functor composition.

\begin{definition}[Properties of asymmetric lenses]
\end{definition}
\begin{center}
\begin{tabular}{l|l}
    Property & Definition \\
    \hline\hline
    \emph{Stable} & $\putl^A(\Id_{A.\getl}) \;=\; \Id_A$
    \\ \hline\emph{PutGet} & $\big(\putl^A\, v\big).\getl \;=\; v$
    \\ \hline\emph{PutPut} & $\putl^A(v \, ; \, w) \;=\; \putl^A\, v \,\, ; \,\, \putl^{\cod{(\putl^A\, v)}}\, w$
    \\ \hline\emph{Well-behaved (wb)} & Stable and PutGet
    \\ \hline\emph{Very wb (vwb)} & Well-behaved and PutPut
  \end{tabular}
\end{center}

The relationships are summarised by the following diagram.
\begin{center}
\begin{tikzpicture}
\coordinate (O) at (0,0);
\draw[fill=red!30] (O) circle (2.8);
\draw[fill=green!40] (O) circle (2);
\draw[fill=yellow!70] (O) circle (1.2);
\draw[fill=blue!45] (O) circle (0.4);

\draw[decoration={text along path,reverse path,text align={align=center},text={vwb}},decorate] (0.5,-.6) arc (0:180:0.5);
\draw[decoration={text along path,reverse path,text align={align=center},text={wb-aLens}},decorate] (0.5,0.2) arc (0:180:0.5);
\draw[decoration={text along path,reverse path,text align={align=center},text={aLens}},decorate] (1.3,0.1) arc (0:180:1.3);
\draw[decoration={text along path,reverse path,text align={align=center},text={Propogators}},decorate] (2.1,0.2) arc (0:180:2.1);
\draw[decoration={text along path,reverse path,text align={align=center},text={Categories}},decorate] (2.9,0) arc (0:180:2.9);
\end{tikzpicture}
\end{center}
\begin{example}
\begin{enumerate}
\item (Behaviour Modelling) Bidirectional Mealy machines.
\item (Cloven Opfibrations) As mentioned in the previous example, cloven
opfibrations yield a lens; it can be readily shown that this is a stable
compositional sectional a-lens.
\end{enumerate}
\end{example}

\subsection{Relationships between lenses} %Pullback Prism}
\label{sec:orge7e93e8}

The relationships between the categories discussed thus far can be summarized by
the \emph{commutative} diagram in Fig.\ref{fig:pbprizm} to be seen as 3D-prism: the back face is a PB-square formed by arrows $\_.\fppg$, $\_.\bppg$, $\_.\Corr$ and $\_.\Corr;\dagger$, while the arrow $\_.\getl$ goes in parallel to the back face. 

\input{figures/pullbackPrism}
%\iffalse The old pullback prism
%\begin{tikzcd}
%         & \ensuremath{Lens} \ar[ddd, "\substack{\textcolor{white}{.} \\[3em] \_.\bppg}"%{very near start}
%         ] \PB \ar[rr, "\_.\fppg"] & & \ensuremath{Ppg} \ar[ddd, "\_.\Corr"]
%\\ \ensuremath{aLens} \ar[rr, near start, rightarrow, "\_.\getl"] \ar[ru, "xxx" , hookrightarrow, ] \ar[ddr, "\_.\putl"] & & \catcat\ \ar[ru, hookrightarrow, "\ensuremath{\iota}"] \ar[rdd, "\mathcal{O}"]
%\\ \\ & \ensuremath{Ppg} \ar[rr, "\,\_.\Corr\, ; \,\dagger"] & & \Span \ensuremath{Set}
%\end{tikzcd}

\zdhide{some details of the diagrams are endinouted here}
\endinput
\noindent Where \(\ensuremath{aLens}\subset \ensuremath{Lens}\) is the view that forgets the functoriality
property of the forward propagator, \ensuremath{\iota} is the ‘functors as propogators’ view
---Example 3.1--- and \ensuremath{\mathcal{O}} is the obvious way to get spans from functors ---see the background section.

Notice that the left and right triangles commute by definitions of the down-right
arrows, \(\putl\) and \ensuremath{\mathcal{O}}: In particular, \((\Corr \circ \ensuremath{\iota})\big(F : \ensuremath{\mathcal{A}} → \ensuremath{\mathcal{B}}\big) =
\big(\Obj \ensuremath{\mathcal{A}} \xleftarrow{\Id} \Obj \ensuremath{\mathcal{A}} \xrightarrow{F} \Obj \ensuremath{\mathcal{B}}\big) = \ensuremath{\mathcal{O}}
\left(F\right)\).

Since the forward propogator of an asymmetric lens is a functor,
applying the functors-as-propogators view performs essentially no change.
Moreover since \(\getl\) is the name of the forward propogator for
asymmetric lenses, we have that the top face of the diagram commutes.

Since \(P.\putl = P.\bppg\) and the corr of \(\putl\) is, by definition, required to
be the inverse of the corr of the \(\getl\), which is just \(\fppg\), we have that
the bottom face of the diagram commutes.

All that remains is the base of the prism.
However, by Theorem 4, this is known to be a pullback square and so commutes.

\fi



